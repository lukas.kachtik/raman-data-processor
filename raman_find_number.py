def find_number(x):
    y = 0
    z = 0
    final_number_position = None
    for i in range(len(x)):
        try:
            int(x[-i - 1])
            y = 1
        except:
            y = 0

        try:
            int(x[-i - 2])
            z = 1
        except:
            z = 0

        if y == 1 and z == 1:
            final_number_position = i
            break

    number = ""
    if final_number_position == None:
        number = None
    else:
        i = final_number_position
        while True:
            try:
                int(x[-i - 1])
                y = 1
            except:
                y = 0

            if y == 0:
                break
            i += 1

        initial_number_position = i - 1


        for i in range(-initial_number_position, -final_number_position + 1):
            number += str(x[i - 1])
        number = int(number)

    return number