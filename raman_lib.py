import matplotlib.pyplot as plt

class Library:

    def __init__(self):
        pass

    def load_files(self, directory_name):
        from os import listdir
        from os.path import isfile, join
        files = [f for f in listdir(str(directory_name)) if isfile(join(str(directory_name), f))]
        return files

    def load_data(self, source_folder, directory, file_name):
        import pandas as pd
        data = pd.read_csv(str(source_folder) + '/' + str(directory) + '/' + str(file_name), sep=";", header=2, names=["x", "y"])

        x = data.x.values.tolist()
        y = data.y.values.tolist()
        self.x = x
        self.y = y

        return x, y

    def make_dirs(self, directory, dir_1 = 'plots'):
        from os import path, makedirs
        if not path.exists(dir_1):
            makedirs(dir_1)
        if not path.exists(str(dir_1) + '/' + str(directory)):
            makedirs(str(dir_1) + '/' + str(directory))
        if not path.exists(str(dir_1) + '/' + str(directory) + '/' + 'PDFs'):
            makedirs(str(dir_1) + '/' + str(directory) + '/' + 'PDFs')

    def remove_x_rays(self, y, med):
        try:
            for i in range(3, len(y) - 3):
                if (y[i] - med) > 30 and abs((y[i] - med) / (y[i - 3] + y[i + 3] - 2 * med)) > 2:
                    y[i] = ((y[i - 2] + y[i + 2]) / 2)
            return y
        except:
            print('Failed removing x-rays')
            return y

    def get_maximum(self, x, y, min_val, max_val):
        x_int = map(int, x)
        i_min = x_int.index(min_val)
        i_max = x_int.index(max_val)

        # TOHLE JDE ZRYCHLIT
        maximum_value = [0, 0]
        i_initial = 0
        for i in range(i_min,i_max):
            if y[i] > maximum_value[1]:
                maximum_value = [x[i], y[i]]
                i_initial= i
        # Vylepsit pro vice stejnych hodnot, ted je to jen pro 2
        # if maximum_value[1] == y[i_initial+1]:
        #     maximum_value[0] = (x[i_initial] + x[i_initial+1]) / 2
        self.i_initial = i_initial
        i_final = i_initial

        for i in range(i_initial,i_max):
            if y[i] == maximum_value[1]:
                i_final = i

        # while maximum_value[1] == y[i_final+1]:
        #     i_final += 1
        maximum_value[0] = (x[i_initial] + x[i_final]) / 2

        self.maximum_value = maximum_value

        return maximum_value

    def peak_integration_2(self, x, y, min_val, max_val):
        x_int = map(int, x)
        i_min = x_int.index(min_val)
        i_max = x_int.index(max_val)
        integrated_intensity = 0.0
        self.x_to_plot = []
        self.y_to_plot = []
        for i in range(i_min, i_max):
            if (y[i] - self.med) > (self.maximum_value[1] - self.med) * 0.5:
                integrated_intensity += (y[i] - self.med)
                self.x_to_plot.append(x[i])
                self.y_to_plot.append(self.med)
                self.x_to_plot.append(x[i])
                self.y_to_plot.append(y[i])
        return integrated_intensity




    def peak_integration(self, y_source):
        i = self.i_initial
        j = self.i_initial

        while (y_source[i] - self.med) > (self.maximum_value[1] - self.med) / 1.1:
            i = i - 1
        i_min = i
        while (y_source[j] - self.med) > (self.maximum_value[1] - self.med) / 1.1:
            j = j + 1
        i_max = j

        width = i_max - i_min

        suma = 0
        for k in range(self.i_initial - width, self.i_initial + width):
            suma = suma + y_source[k]

        # Zkouska plotovani spodni hodnoty
        # plt.plot([self.x[self.i_initial - width], self.x[self.i_initial + width]],
        #          [y_source[self.i_initial - width], y_source[self.i_initial + width]], 'g-')

        # print self.x[self.i_initial - width], self.x[self.i_initial + width], y_source[self.i_initial - width], y_source[self.i_initial + width]
        self.data_to_plot_x = [self.x[self.i_initial - width], self.x[self.i_initial + width]]
        self.data_to_plto_y = [y_source[self.i_initial - width], y_source[self.i_initial + width]]
        return suma


    def median(self, lst):
        lst = sorted(lst)
        n = len(lst)
        if n < 1:
                return None
        if n % 2 == 1:
                self.med =  lst[n//2]
        else:
                self.med =  sum(lst[n//2-1:n//2+1])/2.0
        return self.med

    # def moving_average(self, data, v1, v2, v3):
    #     data_ma = []
    #     for i in range(3, len(data.x) - 3):
    #         data_ma.append((v1 * data.y[i - 3] + v2 * data.y[i - 2] + v3 * data.y[i - 1] + data.y[i] + v3 * data.y[
    #             i - 1] + v2 * data.y[i + 2] + v1 * data.y[i + 3]) / ((v1 + v2 + v3) * 2 + 1))
    #     data_ma2 = []
    #     for i in range(3, len(data_ma) - 3):
    #         data_ma2.append((v1 * data_ma[i - 3] + v2 * data_ma[i - 2] + v3 * data_ma[i - 1] + data_ma[i] + v3 * data_ma[
    #             i - 1] + v2 * data_ma[i + 2] + v1 * data_ma[i + 3]) / ((v1 + v2 + v3) * 2 + 1))
    #     return data_ma2

    def check_background(self, file_name):
        if file_name[0] == "_":
            background = True
        else:
            background = False
        return background

    def adjust_figs(self):
        import matplotlib.pyplot as plt
        plt.subplots_adjust(top=0.95, bottom=0.15, left=0.05, right=0.96, hspace=0.25, wspace=0.35)  #
        fig = plt.gcf()
        fig.set_size_inches(20, 14, forward=True)  #

    def save_figs(self, directory, file_name):
        import matplotlib.pyplot as plt
        plt.savefig('plots/' + str(directory) + '/plot_' + str(file_name) + '.png')
        plt.savefig('plots/' + str(directory) + '/' + 'PDFs' + '/plot_' + str(file_name) + '.pdf')

    def peak_average_fce(self, peak_ratios):
        sum_of_peaks = 0.0
        for i in peak_ratios:
            sum_of_peaks += i
        peak_average = round(sum_of_peaks / len(peak_ratios), 2)
        return peak_average

    def save_data_to_file(self, directory, results):
        peak_ratio_file = open('plots/' + str(directory) + '/peak_ratio.txt', 'w')  #
        for item in results:
            peak_ratio_file.write("%s\n" % item)

    def gaussian_blur(self, y, sigma = 1):
        import scipy.ndimage
        result = scipy.ndimage.filters.gaussian_filter1d(input=y, sigma=sigma, axis=-1, order=0, output=None,
                                                         mode='reflect', cval=0.0, truncate=4.0)
        return result

    def extract_numbers_from_file_name(self, file_name):
        import re
        numbers = re.findall(r'\d+', file_name)
        extracted_data = {}
        for i in numbers:
            if str(i) + 's' in file_name:
                extracted_data['exposure'] = i
            if 'ND' + str(i) in file_name:
                extracted_data['filter'] = 'ND' + str(i)

        if 'exposure' not in extracted_data:
            extracted_data['exposure'] = None
        if 'filter' not in extracted_data:
            extracted_data['filter'] = None

        return extracted_data