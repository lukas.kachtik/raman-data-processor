import matplotlib.pyplot as plt
import numpy as np
from os import listdir, path, makedirs
from os.path import isfile, join
import re
import scipy.ndimage


class Library:

    def __init__(self):
        pass

    def load_files(self, directory_name):
        files = [f for f in listdir(str(directory_name)) if isfile(join(str(directory_name), f))]
        return files

    def load_data(self, source_folder, directory, file_name):
        skip_rows = 0
        while True:
            try:
                data = np.loadtxt(str(source_folder) + '/' + str(directory) + '/' + str(file_name), skiprows=skip_rows)
                break
            except:
                skip_rows += 1

        data = np.transpose(data)
        x = data[0]
        y = data[1]
        return x, y

    def make_dirs(self, directory, dir_1='plots'):
        if not path.exists(dir_1):
            makedirs(dir_1)
        if not path.exists(str(dir_1) + '/' + str(directory)):
            makedirs(str(dir_1) + '/' + str(directory))
        if not path.exists(str(dir_1) + '/' + str(directory) + '/' + 'PDFs'):
            makedirs(str(dir_1) + '/' + str(directory) + '/' + 'PDFs')

    def remove_x_rays(self, y, med):
        try:
            for i in range(3, len(y) - 3):
                if (y[i] - med) > 30 and abs((y[i] - med) / (y[i - 3] + y[i + 3] - 2 * med)) > 2:
                    y[i] = ((y[i - 2] + y[i + 2]) / 2)
            return y
        except:
            print('Failed removing x-rays')
            return y

    # def get_maximum(self, x, y, min_val, max_val):
    #     x_int = list(map(int, x))
    #     i_min = x_int.index(min_val)
    #     i_max = x_int.index(max_val)
    #
    #     # TOHLE JDE ZRYCHLIT
    #     maximum_value = [0, 0]
    #     i_initial = 0
    #     for i in range(i_min, i_max):
    #         if y[i] > maximum_value[1]:
    #             maximum_value = [x[i], y[i]]
    #             i_initial= i
    #     # Vylepsit pro vice stejnych hodnot, ted je to jen pro 2
    #     # if maximum_value[1] == y[i_initial+1]:
    #     #     maximum_value[0] = (x[i_initial] + x[i_initial+1]) / 2
    #     self.i_initial = i_initial
    #     i_final = i_initial
    #
    #     for i in range(i_initial, i_max):
    #         if y[i] == maximum_value[1]:
    #             i_final = i
    #
    #     # while maximum_value[1] == y[i_final+1]:
    #     #     i_final += 1
    #     maximum_value[0] = (x[i_initial] + x[i_final]) / 2
    #     return maximum_value

    def get_peak_properties(self, x, y, x_min, x_max):
        x_int = list(map(int, x))

        index_min = x_int.index(x_min)
        index_max = x_int.index(x_max)

        local_x = x[index_min:index_max]
        local_y = y[index_min:index_max]

        max_intensity = 0.

        for y_val in local_y:
            if y_val > max_intensity:
                max_intensity = y_val

        indices = [i for i, y_val in enumerate(local_y) if y_val == max_intensity]
        sum = 0.
        for index in indices:
            sum += local_x[index]
        max_position = sum / len(indices)

        integrated_intensity = self.peak_integration_2(x, y, x_min, x_max, max_intensity)

        class PeakProperties():
            position = max_position
            maximum = max_intensity
            index = np.where(y == max_intensity)
            intensity = integrated_intensity

        return PeakProperties()

    def peak_integration_2(self, x, y, min_val, max_val, maximum):
        # TODO: It could integrate only the smaller thickness on both sides.
        x_int = list(map(int, x))
        i_min = x_int.index(min_val)
        i_max = x_int.index(max_val)
        integrated_intensity = 0.0
        self.x_to_plot = []
        self.y_to_plot = []
        for i in range(i_min, i_max):
            if (y[i] - self.med) > (maximum - self.med) * 0.5:
                integrated_intensity += (y[i] - self.med)
                self.x_to_plot.append(x[i])
                self.y_to_plot.append(self.med)
                self.x_to_plot.append(x[i])
                self.y_to_plot.append(y[i])
        return integrated_intensity

    def prepare_result_for_saving(self, all_spectra_properties):
        results = [["Name of file",
                    "350 peak position [1/cm]",
                    "420 peak position [1/cm]",
                    "350 peak integrated intensity",
                    "420 peak integrated intensity",
                    "420/350 peak ratio"]]
        for file_name, spectra in zip(all_spectra_properties.keys(), all_spectra_properties.values()):
            results.append([file_name, [spectra[t].position for t in spectra], [spectra[t].intensity for t in spectra],
                            spectra[420].intensity / spectra[350].intensity])

        # for file_name, spectra in zip(all_spectra_properties.keys(), all_spectra_properties.values()):
        #     for pos in spectra:
        #         results.append(spectra[pos].position)
        #     for int in spectra:
        #         results.append(spectra[int].intensity)
        #     results.append(spectra[420].intensity / spectra[350].intensity)


        return results

    def peak_integration(self, y_source, maximum):
        i = self.i_initial
        j = self.i_initial

        while (y_source[i] - self.med) > (maximum.intensity - self.med) / 1.1:
            i = i - 1
        i_min = i
        while (y_source[j] - self.med) > (maximum.intensity - self.med) / 1.1:
            j = j + 1
        i_max = j

        width = i_max - i_min

        suma = 0
        for k in range(self.i_initial - width, self.i_initial + width):
            suma = suma + y_source[k]

        # Zkouska plotovani spodni hodnoty
        # plt.plot([self.x[self.i_initial - width], self.x[self.i_initial + width]],
        #          [y_source[self.i_initial - width], y_source[self.i_initial + width]], 'g-')

        # print self.x[self.i_initial - width], self.x[self.i_initial + width], y_source[self.i_initial - width], y_source[self.i_initial + width]
        self.data_to_plot_x = [self.x[self.i_initial - width], self.x[self.i_initial + width]]
        self.data_to_plto_y = [y_source[self.i_initial - width], y_source[self.i_initial + width]]
        return suma

    def median(self, lst):
        lst = sorted(lst)
        n = len(lst)
        if n < 1:
            return None
        if n % 2 == 1:
            self.med = lst[n // 2]
        else:
            self.med = sum(lst[n // 2 - 1:n // 2 + 1]) / 2.0
        return self.med

    # def moving_average(self, data, v1, v2, v3):
    #     data_ma = []
    #     for i in range(3, len(data.x) - 3):
    #         data_ma.append((v1 * data.y[i - 3] + v2 * data.y[i - 2] + v3 * data.y[i - 1] + data.y[i] + v3 * data.y[
    #             i - 1] + v2 * data.y[i + 2] + v1 * data.y[i + 3]) / ((v1 + v2 + v3) * 2 + 1))
    #     data_ma2 = []
    #     for i in range(3, len(data_ma) - 3):
    #         data_ma2.append((v1 * data_ma[i - 3] + v2 * data_ma[i - 2] + v3 * data_ma[i - 1] + data_ma[i] + v3 * data_ma[
    #             i - 1] + v2 * data_ma[i + 2] + v1 * data_ma[i + 3]) / ((v1 + v2 + v3) * 2 + 1))
    #     return data_ma2

    def check_background(self, file_name):
        return True if file_name[0] == "_" else False

    def adjust_figs(self):
        plt.subplots_adjust(top=0.95, bottom=0.15, left=0.05, right=0.96, hspace=0.25, wspace=0.35)  #
        fig = plt.gcf()
        fig.set_size_inches(20, 14, forward=True)  #

    def save_figs(self, directory, file_name, save_as_png, save_as_pdf):
        if save_as_png:
            plt.savefig('plots/' + str(directory) + '/plot_' + str(file_name) + '.png')
        if save_as_pdf:
            plt.savefig('plots/' + str(directory) + '/' + 'PDFs' + '/plot_' + str(file_name) + '.pdf')

    def peak_average_fce(self, peak_ratios):
        sum_of_peaks = 0.0
        for i in peak_ratios:
            sum_of_peaks += i
        peak_average = round(sum_of_peaks / len(peak_ratios), 2)
        return peak_average

    def save_data_to_file(self, directory, results):
        peak_ratio_file = open('plots/' + str(directory) + '/peak_ratio.txt', 'w')  #
        for item in results:
            peak_ratio_file.write("%s\n" % item)

    def gaussian_blur(self, y, sigma=1):
        result = scipy.ndimage.filters.gaussian_filter1d(input=y, sigma=sigma, axis=-1, order=0, output=None,
                                                         mode='reflect', cval=0.0, truncate=4.0)
        return result

    def store_position_max_value_and_integration(self, maximum, integration_value):
        pass

    def extract_numbers_from_file_name(self, file_name):
        numbers = re.findall(r'\d+', file_name)
        extracted_data = {}
        for i in numbers:
            if str(i) + 's' in file_name:
                extracted_data['exposure'] = i
            if 'ND' + str(i) in file_name:
                extracted_data['filter'] = 'ND' + str(i)

        if 'exposure' not in extracted_data:
            extracted_data['exposure'] = None
        if 'filter' not in extracted_data:
            extracted_data['filter'] = None

        return extracted_data
