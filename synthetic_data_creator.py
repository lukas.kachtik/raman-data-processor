import matplotlib.pyplot as plt

def save(input1, input2):
    file.write(str(input1) + "; " + str(input2) + "\n")

def load_data(file_name):
    import pandas as pd
    data = pd.read_csv(str(file_name), sep=";", header=2, names=["x", "y"])

    x = data.x.values.tolist()
    y = data.y.values.tolist()

    return x, y

def gaussian_blur(y, sigma = 1):
    import scipy.ndimage
    result = scipy.ndimage.filters.gaussian_filter1d(input=y, sigma=sigma, axis=-1, order=0, output=None,
                                                     mode='reflect', cval=0.0, truncate=4.0)
    return result

file_name = "synthetic_data.txt"

file = open(file_name, "w")
for i in range(0,60):
    i = i / 1.0
    x = 0
    # if i > 48 and i < 52:
    #     x = 1.0
    if i == 30:
        x = 1.0
    save(i,x)

file.close()


x, y = load_data(file_name)

plt.plot(x, y, 'r-')


for i in range(1, 10):
    i = i / 1
    y_blurred = gaussian_blur(y, sigma=i)
    plt.plot(x, y_blurred, 'b-')

plt.show()
