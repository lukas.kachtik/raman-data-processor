from time import time

import matplotlib.pyplot as plt
from os import listdir
from sys import stdout
from raman_lib_dev import Library

save_as_png = True
save_as_pdf = False

start_time = time()

lib = Library()
source_folder = 'data'
directories = listdir(source_folder)

n_d = 1

for directory in directories:

    lib.make_dirs(directory)

    files = lib.load_files(str(source_folder) + '/' + str(directory))
    number_of_files = len(files)
    number_of_directories = len(directories)

    results = [["Name of file",
                "350 peak position [1/cm]",
                "420 peak position [1/cm]",
                "350 peak integrated intensity",
                "420 peak integrated intensity",
                "420/350 peak ratio"]]
    n_f = 1
    peak_ratios = []
    integrated_peak_ratios = []
    peak_positions_350 = []
    peak_positions_420 = []

    peak_positions = [350, 420]
    peak_widths = [20, 20]
    index_of_350_peak = 0
    index_of_420_peak = 1

    number_of_peaks = len(peak_positions)

    all_spectra_properties = {}

    for file_name in files:

        x, y = lib.load_data(source_folder, directory, file_name)

        y_source = y

        med = lib.median(y)
        meds = [med] * len(x)
        y = lib.remove_x_rays(y, med)
        y = lib.gaussian_blur(y, sigma=1.1)
        stdout.write('\r' + "[" + str(n_d) + "/" + str(number_of_directories) + "] [" + str(n_f) + "/" + str(
            number_of_files) + "] - " + str(file_name))

        background = lib.check_background(file_name)



        plot_size = 2, number_of_peaks  #
        plt.subplot2grid(plot_size, (0, 0), colspan=number_of_peaks)  #
        plt.plot(x, y, 'r-', x, meds, 'k--')  #
        plt.title(str(file_name))  #
        plt.xlim([200, 700])  #
        plt.xlabel('Raman shift [1/cm]')
        plt.ylabel('Intensity [counts]')

        max_3 = []
        max_3_int = []
        max_3_pos = []

        all_peaks_properties = {}

        for peak_position, i in zip(peak_positions, range(number_of_peaks)):
            if not background:
                x_min = peak_positions[i] - peak_widths[i]  #
                x_max = peak_positions[i] + peak_widths[i]  #

                peak_properties = lib.get_peak_properties(x, y, x_min, x_max)

                # PLOT DATA
                plt.subplot2grid(plot_size, (1, i), colspan=1)  #
                plt.plot(x, y_source, 'r--',
                         x, meds, 'k--',
                         [peak_properties.position, peak_properties.position], [med, peak_properties.maximum], 'k-',
                         x, y, 'b-',
                         lib.x_to_plot, lib.y_to_plot, 'g-')  #

                # DATA DESCRIPTION
                plt.text((x_max + x_min) / 2 - (x_max - x_min) / 2.5,
                         (med - peak_properties.maximum) / 5 + med,
                         'Raman shift: ' + str(round(peak_properties.position, 1)) + ' 1/cm' + '\n' +
                         'Intensity: ' + str(int(peak_properties.maximum - med)) + ' counts')  #

                # PLOT PARAMETERS
                plt.xlim([x_min, x_max])  #
                plt.ylim([(med - peak_properties.maximum) / 3 + med, (peak_properties.maximum - med) * 1.2 + med])  #
                plt.title('Peak position: ' + str(peak_positions[i]) + ' 1/cm')
                plt.xlabel('Raman shift [1/cm]')
                plt.ylabel('Intensity [counts]')

                all_peaks_properties[peak_position] = peak_properties

                max_3.append(float(peak_properties.maximum))  #
                max_3_int.append(peak_properties.intensity)
                max_3_pos.append(float(peak_properties.position))


        if not background:

            all_spectra_properties[file_name] = all_peaks_properties

            # PEAK POSITIONS
            peak_position_350 = round(max_3_pos[index_of_350_peak], 1)
            peak_position_420 = round(max_3_pos[index_of_420_peak], 1)
            peak_positions_350.append(peak_position_350)
            peak_positions_420.append(peak_position_420)

            # PEAK RATIOS
            intensity_350 = max_3[index_of_350_peak] - med
            intensity_420 = max_3[index_of_420_peak] - med
            integrated_intensity_350 = max_3_int[index_of_350_peak]
            integrated_intensity_420 = max_3_int[index_of_420_peak]
            peak_ratio = round(integrated_intensity_420 / integrated_intensity_350, 2)
            integrated_peak_ratio = 0.0
            if integrated_intensity_350 != 0:
                integrated_peak_ratio = round(float(integrated_intensity_420) / integrated_intensity_350, 2)
            peak_ratios.append(peak_ratio)
            integrated_peak_ratios.append(integrated_peak_ratio)

            results.append([file_name,
                            peak_position_350,
                            peak_position_420,
                            round(integrated_intensity_350, 1),
                            round(integrated_intensity_420, 1),
                            integrated_peak_ratio])  #

        # plt.text(med - 125, med - 30, ' Median: ' + str(med)
        # + '                  350/420 intensity ratio: ' + str(round((max_3[2] - med) / (max_3[4] - med),2)))
        if save_as_png or save_as_pdf or True:
            lib.adjust_figs()
            lib.save_figs(directory, file_name, save_as_png, save_as_pdf)
        print (" - DONE")
        n_f += 1

        # print lib.extract_numbers_from_file_name(file_name)



    n_d += 1

    # PEAK POSITION AVERAGES
    peak_position_350_average = lib.peak_average_fce(peak_positions_350)
    peak_position_420_average = lib.peak_average_fce(peak_positions_420)
    results.append("Peak position 350 average: " + str(peak_position_350_average))
    results.append("Peak position 420 average: " + str(peak_position_420_average))

    # PEAK RATIO AVERAGES
    peak_average = lib.peak_average_fce(peak_ratios)
    results.append("Peak ratio average: " + str(peak_average))



    to_be_saved = lib.prepare_result_for_saving(all_spectra_properties)

    # for file_name, spectra in zip(all_spectra_properties.keys(), all_spectra_properties.values()):
    #     print(spectra.keys())

    # lib.save_data_to_file(directory, results)
    lib.save_data_to_file(directory, to_be_saved)

    # print all_spectra_properties[str(files[0])]
    # print all_spectra_properties[str(files[0])][520].index

    # for file_name, spectra in zip(all_spectra_properties.keys(), all_spectra_properties.values()):
    #     print(file_name, [spectra[t].position for t in spectra], [spectra[t].intensity for t in spectra],
    #           spectra[420].intensity / spectra[350].intensity)
        # c = [spectra[t].position for t in spectra]
        # def ahoj(x=0, y=0, z=0, a=0):
        #     print x, y, z
        # ahoj(*c)

    # for file_name, spectra in zip(all_spectra_properties.keys(), all_spectra_properties.values()):
    #     print [properties.position for properties in [spectra.keys()]]
        # for desired_peak, peak_properties in zip(spectra.keys(), spectra.values()):
            # print file_name, desired_peak, peak_properties


print('Time: ' + str(round((time() - start_time), 1)) + ' s.')
