from time import time
import matplotlib.pyplot as plt
from os import listdir
from sys import stdout
from raman_lib import Library

save_figs = True

start = time()

lib = Library()
source_folder = 'data'
directories = listdir(source_folder)

n_d = 1

for directory in directories:

    lib.make_dirs(directory)

    files = lib.load_files(str(source_folder) + '/' + str(directory))
    number_of_files = len(files)
    number_of_directories = len(directories)

    results = [["Name of file",
                "350 peak position [1/cm]",
                "420 peak position [1/cm]",
                "350 peak integrated intensity",
                "420 peak integrated intensity",
                "420/350 peak ratio"]]
    n_f = 1
    peak_ratios = []
    integrated_peak_ratios = []
    peak_positions_350 = []
    peak_positions_420 = []

    for file_name in files:

        x, y = lib.load_data(source_folder, directory, file_name)
        y_source = y

        med = lib.median(y)
        meds = [med] * len(x)
        y = lib.remove_x_rays(y, med)
        y = lib.gaussian_blur(y, sigma=1.1)
        stdout.write('\r' + "[" + str(n_d) + "/" + str(number_of_directories) + "] [" + str(n_f) + "/" + str(
            number_of_files) + "] - " + str(file_name))

        background = lib.check_background(file_name)

        peak_positions = [295, 322, 350, 385, 420]
        peak_widths = [10, 10, 20, 10, 20]
        number_of_peaks = len(peak_positions)

        plot_size = 2, number_of_peaks  #
        plt.subplot2grid(plot_size, (0, 0), colspan=number_of_peaks)  #
        plt.plot(x, y, 'r-', x, meds, 'k--')  #
        plt.title(str(file_name))  #
        plt.xlim([200, 700])  #
        plt.xlabel('Raman shift [1/cm]')
        plt.ylabel('Intensity [counts]')

        max_3 = []
        max_3_int = []
        max_3_pos = []

        for i in range(number_of_peaks):
            if not background:
                x_min = peak_positions[i] - peak_widths[i]  #
                x_max = peak_positions[i] + peak_widths[i]  #

                max_2 = lib.get_maximum(x, y, x_min, x_max)  #
                # peak_integration_value = lib.peak_integration(y_source)
                peak_integration_value = lib.peak_integration_2(x, y, x_min, x_max)
                plt.subplot2grid(plot_size, (1, i), colspan=1)  #

                # PLOT DATA
                plt.plot(x, y_source, 'r--',
                         x, meds, 'k--',
                         [max_2[0], max_2[0]], [med, max_2[1]], 'k-',
                         x, y, 'b-',
                         lib.x_to_plot, lib.y_to_plot, 'g-')  #

                # DATA DESCRIPTION
                plt.text((x_min + x_max) / 2 - (x_max - x_min) / 2.5,
                         (med - max_2[1]) / 5 + med,
                         'Raman shift: ' + str(round(max_2[0], 1)) + ' 1/cm' + '\n' +
                         'Intensity: ' + str(int(max_2[1] - med)) + ' counts')  #

                # PLOT PARAMETERS
                plt.xlim([x_min, x_max])  #
                plt.ylim([(med - max_2[1]) / 3 + med, (max_2[1] - med) * 1.2 + med])  #
                plt.title('Peak position: ' + str(peak_positions[i]) + ' 1/cm')
                plt.xlabel('Raman shift [1/cm]')
                plt.ylabel('Intensity [counts]')

                max_3.append(float(max_2[1]))  #
                max_3_int.append(peak_integration_value)
                max_3_pos.append(float(max_2[0]))


        if not background:
            # PEAK POSITIONS
            peak_position_350 = round(max_3_pos[2], 1)
            peak_position_420 = round(max_3_pos[4], 1)
            peak_positions_350.append(peak_position_350)
            peak_positions_420.append(peak_position_420)

            # PEAK RATIOS
            intensity_350 = max_3[2] - med
            intensity_420 = max_3[4] - med
            integrated_intensity_350 = max_3_int[2]
            integrated_intensity_420 = max_3_int[4]
            peak_ratio = round(integrated_intensity_420 / integrated_intensity_350, 2)
            integrated_peak_ratio = 0.0
            if integrated_intensity_350 != 0:
                integrated_peak_ratio = round(float(integrated_intensity_420) / integrated_intensity_350, 2)
            peak_ratios.append(peak_ratio)
            integrated_peak_ratios.append(integrated_peak_ratio)

            results.append([file_name,
                            peak_position_350,
                            peak_position_420,
                            round(integrated_intensity_350, 1),
                            round(integrated_intensity_420, 1),
                            integrated_peak_ratio])  #

        # plt.text(med - 125, med - 30, ' Median: ' + str(med)
        # + '                  350/420 intensity ratio: ' + str(round((max_3[2] - med) / (max_3[4] - med),2)))
        if save_figs:
            lib.adjust_figs()
            lib.save_figs(directory, file_name)
        print (" - DONE")
        n_f += 1

        # print lib.extract_numbers_from_file_name(file_name)

    n_d += 1

    # PEAK POSITION AVERAGES
    peak_position_350_average = lib.peak_average_fce(peak_positions_350)
    peak_position_420_average = lib.peak_average_fce(peak_positions_420)
    results.append("Peak position 350 average: " + str(peak_position_350_average))
    results.append("Peak position 420 average: " + str(peak_position_420_average))

    # PEAK RATIO AVERAGES
    peak_average = lib.peak_average_fce(peak_ratios)
    results.append("Peak ratio average: " + str(peak_average))

    lib.save_data_to_file(directory, results)

# print 'Time: ' + str(round((time() - start), 1)) + ' s.'
